# Docker_Symfony

+ Install Docker Environnement for Symfony 
    - mysql
    - phpmyadmin
    - maildev
    - www
+ Add configuration of gitlab-CI for Unit Testing, using jakzal/phpqa image with stages :
    - SecurityChecker
    - CodingStandards
    - UnitTests
